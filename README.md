# color-detection-functions

Aplication is based on detecting colors from image.
It's split on two parts:

- **Serverless functions** - aplication logic, color detecting [./functions](https://gitlab.com/mockup-functions/color-detection-functions/-/tree/master/functions)
- **Client application** - easy example of usage [./public](https://gitlab.com/mockup-functions/color-detection-functions/-/tree/master/public)

## Getting started

At first, clone the project:

```
git clone https://gitlab.com/mockup-functions/color-detection-functions
```

Install dependencies for both parts:

```
cd ./functions
npm install

cd ./public/dev
npm install
```

Build the client solution:

```
cd ./public/dev
npm run build
```

Deploy application to firebase:

```
// if project hasn't been initialized yet
firebase init

firebase deploy
```

## Demo

You can see how application works [here](https://colour-detector-function.web.app/).

## Authors

- **Radek Kučera** - _Aplication logic and initial source_ - [link](https://gitlab.com/radek.kucera)
