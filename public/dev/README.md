# Image Color Detector

It's a React component used for generating color palette from image.

## Getting started

If you want to use the component, you can simply import it to your project:

```
import ImageColorDetector from "./imageColorDetector/imageColorDetector.js";
```

and use it as a typical component:

```
...
render() {
    return <ImageColorDetector/>
}
...
```

to get back into main page (uploading), simply change the props view to 0:

```
...
    return <ImageColorDetector view={this.state.back /* - 0 */}>
...
```

If you want to change CSS or Configs, go to ./imageColorDetector and rewrite it.
Make sure you changed the API_url to your own API, actuall address is just an example.

## Authors

- **Radek Kučera** - _Aplication logic and initial source_ - [link](https://gitlab.com/radek.kucera)
