import React from "react";

function ImageColorDetectorResult(props) {
  const colors = props.colors.map((color) => {
    return (
      <span
        role="img"
        className="image-color-detector-section-body-color"
        key={"CLR_" + color}
        alt={color}
        style={{ backgroundColor: color }}
      ></span>
    );
  });
  return (
    <div className="image-color-detector-section">
      <span className="image-color-detector-section-title">
        <img
          src={props.image}
          alt="Detection image"
          className="image-color-detector-section-title-image"
        ></img>
      </span>
      <span className="image-color-detector-section-body">{colors}</span>
    </div>
  );
}

export default ImageColorDetectorResult;
