export const compretion_options = {
  maxSizeMB: 1,
  maxWidthOrHeight: 512,
  useWebWorker: true,
};

export const API_url =
  "https://us-central1-colour-detector-function.cloudfunctions.net/getImagePalette";
