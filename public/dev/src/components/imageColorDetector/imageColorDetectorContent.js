import React from "react";

function ImageColorDetectorContent(props) {
  return (
    <div className="image-color-detector-section">
      <span className="image-color-detector-section-title">
        <h1>Image color detector</h1>
      </span>
      <span className="image-color-detector-section-body">
        <p>{props.status}</p>
      </span>
    </div>
  );
}

export default ImageColorDetectorContent;
