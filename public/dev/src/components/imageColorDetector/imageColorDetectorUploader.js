import React from "react";

function ImageColorDetectorUploader(props) {
  return (
    <div className="image-color-detector-section">
      <span className="image-color-detector-section-title">
        <h1>Image color detector</h1>
      </span>
      <span className="image-color-detector-section-body">
        <label
          htmlFor="file"
          className="image-color-detector-section-body-input-label"
        >
          Upload image
        </label>
        <input
          type="file"
          name="file"
          id="file"
          accept="image/png,image/jpeg"
          className="image-color-detector-section-body-input"
          onChange={(e) => props.onUpload(e)}
        />
      </span>
    </div>
  );
}

export default ImageColorDetectorUploader;
