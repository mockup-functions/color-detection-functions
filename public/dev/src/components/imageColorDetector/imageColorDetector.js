import React from "react";
import imageCompression from "browser-image-compression";

import ImageColorDetectorContent from "./imageColorDetectorContent";
import ImageColorDetectorUploader from "./imageColorDetectorUploader";
import {
  compretion_options as OPTIONS,
  API_url as API_URL,
  API_url,
} from "./imageColorDetectorConfigs";
import ImageColorDetectorResult from "./imageColorDetectorResult";
import "./imageColorDetector.css";

class ImageColorDetector extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      view: this.props.view ? this.props.view : 0,
      image: null,
      status: "",
    };

    this.handleUpload = this.handleUpload.bind(this);
  }

  handleUpload(event) {
    // Bad babel version doesnt support async/await syntax for some reason.
    // lets get back into ES5 and use .then() cascade, yaaay!
    // https://github.com/babel/babel/issues/9849
    const image = event.target.files[0];
    this.setState({ view: 1, status: "Compressing file..." });

    try {
      // Image compression
      imageCompression(image, OPTIONS)
        .then((compressedImage) => {
          // Converting blob to file
          compressedImage = new File([compressedImage], image.name, {
            lastModifiedDate: image.date,
            type: image.type,
          });

          // Saving image
          this.setState({
            image: URL.createObjectURL(compressedImage),
            status: "Detecting colors...",
          });

          // Generating form data and adding file
          const form = new FormData();
          form.append("file", compressedImage);

          // Uploading file to server
          const response = fetch(API_url, {
            method: "POST",
            body: form,
          });

          // Response handle
          response
            .then((res) => res.json())
            .then((result) => {
              if (result.status) {
                this.setState({ view: 2, colors: result.colors });
              } else {
                this.setState({ view: 1, status: result.error });
              }
            })
            .catch((error) => {
              this.setState({
                view: 1,
                status: "Error with image uploading!",
              });

              setTimeout(() => {
                this.setState({
                  view: 0,
                  image: null,
                  status: "",
                });
              }, 3000);
            });
        })
        .catch((err) => {
          this.setState({ view: 1, status: "Error with image compression!" });

          setTimeout(() => {
            this.setState({
              view: 0,
              image: null,
              status: "",
            });
          }, 3000);
        });
    } catch (err) {
      this.setState({ view: 1, status: "Error with image uploading!" });

      setTimeout(() => {
        this.setState({
          view: 0,
          image: null,
          status: "",
        });
      }, 3000);
    }
  }

  render() {
    const view = () => {
      // Main page
      if (this.state.view === 0) {
        return (
          <ImageColorDetectorUploader
            onUpload={(e) => this.handleUpload(e)}
          ></ImageColorDetectorUploader>
        );
        // Image is in processing, uploading, or error
      } else if (this.state.view === 1) {
        return (
          <ImageColorDetectorContent
            status={this.state.status}
          ></ImageColorDetectorContent>
        );
        // Image result
      } else if (this.state.view === 2) {
        return (
          <ImageColorDetectorResult
            image={this.state.image}
            colors={this.state.colors}
          ></ImageColorDetectorResult>
        );
      }
    };
    return <div className="image-color-detector shadow">{view()}</div>;
  }
}

export default ImageColorDetector;
