const getPixels = require("get-pixels");

/**
 * @module ImageProcessor
 * @description It's class used for image processing and working with colors.
 */
class ImageProcessor {
  /**
   * @constructor
   * @param {string} [path] - Path where image is stored.
   * @param {number=} [pixelDepth=10] - It represents quality of image reading.
   * @param {number=} [trash=15] - It represents difference between "close" colors.
   */
  constructor(path, pixelDepth, trash) {
    this.path = path;
    this.pixelDepth = pixelDepth ? pixelDepth : 10;
    this.trash = trash ? trash : 15;
  }

  /**
   * @description It's reading image file and generating informations about it.
   * @returns {Promise} - Informations about resolution, color pixels etc.
   */
  getImage() {
    return new Promise((resolve, reject) => {
      getPixels(this.path, (err, data) => {
        if (err) reject(err);
        resolve(data);
      });
    });
  }

  /**
   * @description It's creating array of pixels that are included in image file.
   * @param {Object} [image] - Informations about readed file, can be get by getImage().
   * @param {number=} [resolution] - Number of pixels in image (width * height).
   * @returns {Array} - Array of pixels - [[0, 0, 0], [255, 255, 255]].
   */
  getPixelCollection(image, resolution) {
    resolution = resolution || image.shape[0] * image.shape[1];
    let pixelCollection = [];
    let pixels = image.data;

    for (
      let i = 0, offset, r, g, b, a;
      i < resolution;
      i = i + this.pixelDepth
    ) {
      offset = i * 4;
      r = pixels[offset + 0];
      g = pixels[offset + 1];
      b = pixels[offset + 2];
      a = pixels[offset + 3];

      if (typeof a === "undefined" || a >= 125) {
        // Uncomment this to not allow white
        //if (!(r > 250 && g > 250 && b > 250)) {
        pixelCollection.push([r, g, b]);
        //}
      }
    }

    return pixelCollection;
  }

  /**
   * @description It's detecting the most used colors in the picture.
   * @param {number=} [colors=10] - Number of detected colors.
   * @returns {Promise} - Sorted array of colors.
   */
  getDetectedColors(colors) {
    colors = colors || 10;

    const result = new Promise(async (resolve, reject) => {
      try {
        const image = await this.getImage();
        const resolution = image.shape[0] * image.shape[1];
        const pixels = this.getPixelCollection(image, resolution);
        const colorMap = this.sortByCount(pixels);
        const trashFilter = this.filterClosePixels(colorMap);
        const finalColors = trashFilter.slice(0, colors);
        // Uncomment this to dont use background color as the most dominant
        /*
        if (this.isColorBackground(pixels, image.shape[0], trashFilter[0])) {
          finalColors.push(trashFilter[0]);
          finalColors.shift();
        }
        */
        resolve(finalColors);
      } catch (e) {
        reject(e);
      }
    });

    return result;
  }

  /**
   * @description It's converting rgb() color to #hex color.
   * @param {number} [r] - Red color.
   * @param {number} [g] - Green color.
   * @param {number} [b] - Blue color.
   * @returns {string} Converted color (#ffffff).
   */
  rgbToHex(r, g, b) {
    return (
      "#" +
      [r, g, b]
        .map((x) => {
          const hex = x.toString(16);
          return hex.length === 1 ? "0" + hex : hex;
        })
        .join("")
    );
  }

  /**
   * @description It's sorting array by count of element and filtering duplicates.
   * @param {Array} [array] - Array that you wanted to be sorted.
   * @returns {Array} - Sorted and filtered array.
   */
  sortByCount(array) {
    const map = array.reduce((item, index) => {
      item[index] = (item[index] || 0) + 1;
      return item;
    }, {});

    const sort = Object.keys(map).sort((a, b) => {
      return map[b] - map[a];
    });

    return sort.map((item) => {
      return JSON.parse("[" + item + "]");
    });
  }

  /**
   * @description It's filtering close colors.
   * @param {Array} [colorMap] - Colors in image sorted by count.
   * @returns {Array} Filtered array of colors in image.
   */
  filterClosePixels(colorMap) {
    let newColorMap = [colorMap[0]];

    for (let i = 1; i < colorMap.length; i++) {
      const child = colorMap[i];
      let isClose = false;

      for (let j in newColorMap) {
        const parrent = newColorMap[j];
        let dif =
          Math.abs(parrent[0] - child[0]) +
          Math.abs(parrent[1] - child[1]) +
          Math.abs(parrent[2] - child[2]);
        if (dif <= this.trash) {
          isClose = true;
          break;
        }
      }

      if (!isClose) {
        newColorMap.push(child);
      }
    }

    return newColorMap;
  }

  /**
   * @description Checks if color is the background color.
   * @param {Array} [pixels] - Pixels in image.
   * @param {number} [width] - Image width.
   * @param {Array} [color] - Color that we want to check.
   * @returns {boolean} - True if the color is background color.
   */
  isColorBackground(pixels, width, color) {
    const shapes = [
      pixels[0],
      pixels[0 + width],
      pixels[pixels.length - width - 1],
      pixels[pixels.length - 1],
    ];

    const colorInShapes = shapes.filter(
      (item) => JSON.stringify(item) === JSON.stringify(color)
    ).length;

    return colorInShapes > 2;
  }

  /**
   * @description It's detecting the most used colors in the picture.
   * @param {number} [colors] - Number of detected colors.
   * @returns {Promise} - Sorted array of colors in hex.
   */
  async getDetectedColorsInHex(colors) {
    const result = await this.getDetectedColors(colors);
    return result.map((item) => {
      return this.rgbToHex(item[0], item[1], item[2]);
    });
  }
}

module.exports = ImageProcessor;
