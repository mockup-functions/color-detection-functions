const assert = require("assert");
const expect = require("chai").expect;

const ImageProcessor = require("./imageProcessor");
const COLOR_PREDICTION = [
  [119, 226, 109],
  [166, 115, 206],
  [26, 24, 137],
  [75, 30, 82],
  [168, 47, 47],
  [6, 202, 215],
  [252, 101, 155],
  [223, 239, 128],
  [53, 84, 48],
  [0, 0, 0],
];

describe("Image processing library", () => {
  let imageProcessor = new ImageProcessor("./bad_test.png", 30);
  describe("Image loading from path", () => {
    it("should handle error when path not exist", () => {
      imageProcessor.getImage().catch((err) => {
        assert.ifError(err);
      });
    });

    imageProcessor.path = "./test.png";
    it("should load image", () => {
      imageProcessor.getImage().then((image) => {
        assert.equal(!!image.data, true);
      });
    });
  });

  describe("Creating pixel collection from image", () => {
    it("should create array of pixels", async () => {
      let image = await imageProcessor.getImage();
      const pixels = imageProcessor.getPixelCollection(image);
      console.log(pixels);
      expect(pixels).to.be.an("array");
    });
  });

  describe("Detecting colors from image", () => {
    it("should return array of colors", async () => {
      let colors = await imageProcessor.getDetectedColors(10);
      expect(colors).to.be.an("array");
    });

    it("should find 10 different colors", async () => {
      let colors = await imageProcessor.getDetectedColors(10);
      expect(colors).to.have.lengthOf(10);
    });

    it("should be sorted by sorted by dominant color", async () => {
      let colors = await imageProcessor.getDetectedColors(10);
      expect(JSON.stringify(colors)).to.be.equal(
        JSON.stringify(COLOR_PREDICTION)
      );
    });
  });

  describe("Converting colors to hex", () => {
    it("should convert rgb color to hex", () => {
      expect(imageProcessor.rgbToHex(119, 226, 109)).to.be.equal("#77e26d");
    });

    it("should get array of colors in hex", async () => {
      let colors = await imageProcessor.getDetectedColorsInHex(10);
      expect(colors).to.be.an("array").that.contains("#77e26d");
    });
  });
});
