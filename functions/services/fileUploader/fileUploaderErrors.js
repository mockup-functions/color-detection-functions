const errors = {
  badMethod: {
    code: 405,
    response: { status: false, error: "Method not allowed, use POST instead!" }
  },
  emptyFile: {
    code: 400,
    response: { status: false, error: "Request without file!" }
  },
  multipleFiles: {
    code: 400,
    response: { status: false, error: "Too many files detected!" }
  },
  badType: {
    code: 400,
    response: { status: false, error: "Wrong file type submitted!" }
  },
  fileSizeLimit: {
    code: 400,
    response: {
      status: false,
      error: "File size is bigger than supported maximum!"
    }
  },
  nonExistingContext: {
    code: 400,
    response: { status: false, error: "Request is missing file argument!" }
  },
  imageNotValid: {
    code: 400,
    response: { status: false, error: "Image is not valid!" }
  },
  uploadFailed: {
    code: 400, 
    response: { status: false, error: "Upload were not succesfull!"}
  }
};

module.exports = errors;
