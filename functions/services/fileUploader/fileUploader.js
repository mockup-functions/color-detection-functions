const BusBoy = require("busboy");
const path = require("path");
const os = require("os");
const fs = require("fs");
const ERR = require("./fileUploaderErrors");
const supportedFormats = ["image/jpeg", "image/jpg", "image/png"];

/**
 * @module FileUploader
 * @description It's class used for uploading files to server temp directory.
 */
class FileUploader {
  /**
   * @constructor
   * @param {Object} [req] - Http request.
   * @param {Object} [res] - Http response.
   */
  constructor(req, res) {
    this.req = req;
    this.res = res;
    this.busboy = this._isRequestValid();
    this.tmpdir = os.tmpdir();
    this.fields = {};
    this.uploads = {};
    this.filePath = "";
    this._handlerMiddleware();
  }

  _isRequestValid() {
    try {
      return new BusBoy({
        headers: this.req.headers,
        limits: {
          limit: { files: 1, fileSize: 8 * 1024 * 1024 },
        },
      });
    } catch (err) {
      return this.res
        .status(ERR.nonExistingContext.code)
        .json(ERR.nonExistingContext.response)
        .end();
    }
  }

  _handlerMiddleware() {
    this._handlerLimit();
    this._handlerField();
    this._handlerFile();
    this.busboy.end(this.req.rawBody);
  }

  _handlerLimit() {
    this.busboy.on("limits", () => {
      this.res
        .status(ERR.fileSizeLimit.code)
        .json(ERR.fileSizeLimit.response)
        .end();
    });
  }

  _handlerError(fileName, type) {
    this._checkIfEmptyField(type);
    this._checkIfMultipleFields(fileName);
    this._checkIfFileTypeValid(type);
  }

  _handlerField() {
    this.busboy.on("field", (fieldName, value) => {
      this.fields[fieldName] = value;
    });
  }

  _handlerFile() {
    const fileWrites = [];

    this.busboy.on("file", (fieldName, file, fileName, encoding, mimeType) => {
      try {
        this._handlerError(fileName, mimeType);

        const filePath = path.join(this.tmpdir, fileName);
        this.uploads[fieldName] = filePath;

        this.filePath = filePath;

        const writeStream = fs.createWriteStream(filePath);
        file.pipe(writeStream);

        const promise = new Promise((resolve, reject) => {
          file.on("end", () => {
            writeStream.end();
          });

          writeStream.on("finish", resolve);
          writeStream.on("error", reject);
        });

        fileWrites.push(promise);
        return fileWrites;
      } catch (err) {
        this.res.status(ERR.uploadFailed.code).json(ERR.uploadFailed.response);
      }
    });
  }

  _checkIfEmptyField(type) {
    if (!type || type === "text/plain") {
      return this.res
        .status(ERR.emptyFile.code)
        .json(ERR.emptyFile.response)
        .end();
    }
  }

  _checkIfMultipleFields(fileName) {
    if (typeof fileName !== "string" && fileName.length > 1) {
      return this.res
        .status(ERR.multipleFiles.code)
        .json(ERR.multipleFiles.response)
        .end();
    }
  }

  _checkIfFileTypeValid(type) {
    if (!supportedFormats.includes(type)) {
      return this.res.status(ERR.badType.code).json(ERR.badType.response).end();
    }
  }

  /**
   * @description Definition of what to do after file has been uploaded.
   * @param {Function} [callback] - Function to call after file uploaded.
   * @returns {string} - File path.
   */
  onFinish(callback) {
    this.busboy.on("finish", () => {
      callback(this.filePath);
    });
  }
}

module.exports = FileUploader;
