const functions = require("firebase-functions");
const ImageProcessor = require("./services/imageProcessor/imageProcessor");
const ERR = require("./services/fileUploader/fileUploaderErrors");
const FileUploader = require("./services/fileUploader/fileUploader");

exports.getImagePalette = functions.https.onRequest((req, res) => {
  // ! --- DELETE THIS IN PRODUCTION ---
  res.set("Access-Control-Allow-Origin", "*");

  // Check if method is post
  if (req.method !== "POST") {
    res.status(ERR.badMethod.code).json(ERR.badMethod.response).end();
  }

  // Creating new upload service
  const uploader = new FileUploader(req, res);
  // After file is uploaded
  uploader.onFinish(async (path) => {
    try {
      // Creating new image processor with path,
      // quality 20 (skipping 20 pixels to make code faster),
      // thrash hold 15 (definition of close images - 15 - 255, 255, 255 is close to 255, 250, 245)
      const processor = new ImageProcessor(path, 20, 15);
      // Detecting 10 colors from image
      const colors = await processor.getDetectedColorsInHex(10);
      res.status(200).json({ status: true, colors: colors });
    } catch (e) {
      res.status(ERR.imageNotValid.code).json(ERR.imageNotValid.response);
    }
  });
});
